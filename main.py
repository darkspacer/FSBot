import random
from aiogram import Bot
from aiogram.dispatcher import FSMContext
from aiogram.utils import executor
from aiogram.dispatcher.filters.state import State, StatesGroup
from aiogram.dispatcher.filters import BoundFilter
import db
import keyboards as kb
from string import ascii_letters, digits
import logging 
from asyncio import sleep
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram import Dispatcher, types
from aiogram.dispatcher.handler import CancelHandler
from aiogram.dispatcher.middlewares import BaseMiddleware
from aiogram.utils import exceptions
from aiogram.utils.exceptions import Throttled
import os
from dotenv import load_dotenv


load_dotenv() 
logging.basicConfig(level=logging.INFO)

bot = Bot(token=os.getenv("TOKEN"))
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage)

class ThrottlingMiddleware(BaseMiddleware):
    """
    Simple middleware
    """
 
    def __init__(self, limit=2, key="antiflood__message"):
        self.limit = limit
        self.key = key
        super(ThrottlingMiddleware, self).__init__()
 
    async def on_process_message(self, message: types.Message, data: dict):
        # if message.from_user.id in self.config.admins:
        #     return
 
        dispatcher = Dispatcher.get_current()
        try:
            await dispatcher.throttle(self.key, rate=self.limit)
        except Throttled as t:
            if t.exceeded_count <= 2:
                try:
                    await message.answer("Не флуди >_<\nя перестану отвечать")
                except exceptions.BadRequest:
                    pass
 
            raise CancelHandler()

class IsPrivate(BoundFilter):
    async def check(self, message: types.Message):
        return message.chat.type == types.ChatType.PRIVATE
class Info(StatesGroup):
    upload_file = State()
    upload_file_password = State()
    delete_file = State()
    check_password = State()
    ad = State()


@dp.message_handler(IsPrivate(), commands=['kb'])
async def kb_hide(message: types.Message):
    if message.get_args() == "hide":
        await bot.send_message(chat_id=message.chat.id, text="Клавиатура убрана\nно её можно вернуть используя /kb\nили снова ввести /start",reply_markup=kb.delete_kb())
    else:
        await bot.send_message(chat_id=message.chat.id, text='Клавиатура:', reply_markup=kb.menu_kb())
        await sleep(5)
        await message.delete()

##############################################################

@dp.message_handler(IsPrivate(), commands=['start'], state='*')
async def start_command(message: types.Message, state: FSMContext):
    args = message.get_args()
    bot_data = await bot.get_me()
    bot_data['username']
    if db.get_users_exist(message.chat.id) is False:
        db.add_user_to_db(message.chat.id)
        if not args:
            await bot.send_message(message.chat.id,f'Добро пожаловать, {message.from_user.first_name}!\nДержи клавиатуру',reply_markup=kb.menu_kb())
        else:
            type_file, fileID,password = db.get_file(args)
            if type_file is None and fileID is None:
                await bot.send_message(chat_id=message.chat.id, text='Я не нашел данный файл:(', reply_markup = kb.menu_kb())
            else:
                if password == (None,):
                    if type_file[0] == 'фото':
                        await bot.send_photo(chat_id=message.chat.id, photo=fileID[0])
                    elif type_file[0] == 'видео':
                        await bot.send_video(chat_id=message.chat.id, video=fileID[0])
                    elif type_file[0] == 'голос':
                        await bot.send_voice(chat_id=message.chat.id, voice=fileID[0])
                    elif type_file[0] == 'документ':
                        await bot.send_document(chat_id=message.chat.id, document=fileID[0])
                else:
                    await bot.send_message(chat_id=message.chat.id, text='Упс, кажется файл защищен паролем, введите пароль:', reply_markup = kb.back_kb())
                    await state.update_data(check_password=args)
                    await Info.check_password.set()
    else:
        if not args:
            await bot.send_message(message.chat.id,f'Добро пожаловать, {message.from_user.first_name}!\nДержи клавиатуру',reply_markup=kb.menu_kb())
        else:
            type_file, fileID,password = db.get_file(args)
            if type_file is None and fileID is None:
                await bot.send_message(chat_id=message.chat.id, text='Я не нашел данный файл :(')
            else:
                if password == (None,):
                    if type_file[0] == 'фото':
                        await bot.send_photo(chat_id=message.chat.id, photo=fileID[0])
                    elif type_file[0] == 'видео':
                        await bot.send_video(message.chat.id,video=fileID[0])
                    elif type_file[0] == 'голос':
                        await bot.send_voice(chat_id=message.chat.id, voice=fileID[0])
                    elif type_file[0] == 'документ':
                        await bot.send_document(chat_id=message.chat.id, document=fileID[0])
                else:
                    await bot.send_message(chat_id=message.chat.id, text='Упс, кажется файл защищен паролем, введите пароль:', reply_markup = kb.back_kb())
                    await state.update_data(check_password=args)
                    await Info.check_password.set()

@dp.message_handler(state=Info.check_password, content_types=types.ContentTypes.ANY)
async def upload_file(message: types.Message, state: FSMContext):
    bot_data = await bot.get_me()
    bot_data['username']
    if message.text:
        if message.text.lower() == 'отмена':
            await bot.send_message(chat_id=message.chat.id, text='Отменено.', reply_markup=kb.menu_kb())
            await state.finish()
        else:
            user_data = await state.get_data()
            code = user_data['check_password']
            type_file, fileID, password = db.get_file(code)
            if message.text == password[0]:
                if type_file[0] == "фото":
                    await bot.send_photo(chat_id=message.chat.id, photo=fileID[0])
                elif type_file[0] == "видео":
                    await bot.send_video(chat_id=message.chat.id, video=fileID[0])
                elif type_file[0] == "голос":
                    await bot.send_voice(chat_id=message.chat.id, voice=fileID[0])
                elif type_file[0] == "документ":
                    await bot.send_document(chat_id=message.chat.id, document=fileID[0])
                await state.finish()
            else:
                await bot.send_message(chat_id=message.chat.id, text='Упс, это не верный пароль, попробуй еще раз:')
    else:
        await bot.send_message(chat_id=message.chat.id, text='Упс, это не верный пароль, попробуй еще раз:')

async def SMF(message):
    await bot.send_message(chat_id=message.chat.id, text='Отправь мне файл', reply_markup=kb.back_kb())
    await Info.upload_file.set()


@dp.message_handler(text="Загрузить файл")
async def upload_file(message: types.Message):
    if db.get_users_exist(message.chat.id) is True:
        await SMF(message)

@dp.message_handler(text="Мои файлы")
async def my_files(message: types.Message):
    if db.get_users_exist(message.chat.id) is True:
        bot_data = await bot.get_me()
        bot_name = bot_data['username']
        all_types, all_ids, passwords = db.get_files_user(message.from_user.id)
        if all_types == []:
            await bot.send_message(message.chat.id, text='У вас нету загруженных файлов, чтобы загрузить файлы нажмите "Загрузить файл"', reply_markup = kb.menu_kb())
        else:
            text='Ваши файлы: \n\n'
            for i, id_file in enumerate(all_ids):
                text+=f'{i+1}. https://t.me/{str(bot_name)}?start={id_file[0]} | {all_types[i][0]} | Пароль: {passwords[i][0]}\n\n'
            await bot.send_message(chat_id=message.chat.id, text=text, reply_markup = kb.delete_file())
            
@dp.message_handler(state=Info.upload_file_password, content_types=types.ContentTypes.TEXT)
async def upload_file(message: types.Message, state: FSMContext):
    bot_data = await bot.get_me()
    bot_name = bot_data['username']
    user_data = await state.get_data()
    file_data = user_data['upload_file_password']
    if message.text == '-':
        if file_data.split('|')[1] == "фото":
            code = file_data.split('|')[2]
            db.add_new_file(file_data.split('|')[0], "фото", file_data.split('|')[2], file_data.split('|')[3])
            await bot.send_message(chat_id=message.chat.id, text=f'Файл был успешно загружен.\n\nhttps://t.me/{bot_name}?start={code}', reply_markup=kb.menu_kb())
            await state.finish()
        elif file_data.split('|')[1] == "видео":
            code = file_data.split('|')[2]
            db.add_new_file(file_data.split('|')[0], "видео", file_data.split('|')[2], file_data.split('|')[3])
            await bot.send_message(chat_id=message.chat.id, text=f'Файл был успешно загружен.\n\nhttps://t.me/{bot_name}?start={code}', reply_markup=kb.menu_kb())
            await state.finish()
        elif file_data.split('|')[1] == "голос":
            code = file_data.split('|')[2]
            db.add_new_file(file_data.split('|')[0], "голос", file_data.split('|')[2], file_data.split('|')[3])
            await bot.send_message(chat_id=message.chat.id, text=f'Файл был успешно загружен.\n\nhttps://t.me/{bot_name}?start={code}', reply_markup=kb.menu_kb())
            await state.finish()
        elif file_data.split('|')[1] == "документ":
            code = file_data.split('|')[2]
            db.add_new_file(file_data.split('|')[0], "документ", file_data.split('|')[2], file_data.split('|')[3])
            await bot.send_message(chat_id=message.chat.id, text=f'Файл был успешно загружен.\n\nhttps://t.me/{bot_name}?start={code}', reply_markup=kb.menu_kb())
            await state.finish()
    elif message.text.lower() == 'отмена':
        await bot.send_message(chat_id=message.chat.id, text='Вы вернулись в главное меню.', reply_markup=kb.menu_kb())
        await state.finish()
    else:
        if file_data.split('|')[1] == "фото":
            code = file_data.split('|')[2]
            db.add_new_file_with_password(file_data.split('|')[0], "фото", file_data.split('|')[2], file_data.split('|')[3], message.text)
            await bot.send_message(chat_id=message.chat.id, text=f'Файл был успешно загружен.\n\nhttps://t.me/{bot_name}?start={code}', reply_markup=kb.menu_kb())
            await state.finish()
        elif file_data.split('|')[1] == "видео":
            code = file_data.split('|')[2]
            db.add_new_file_with_password(file_data.split('|')[0], "видео", file_data.split('|')[2], file_data.split('|')[3], message.text)
            await bot.send_message(chat_id=message.chat.id, text=f'Файл был успешно загружен.\n\nПароль: {message.text}\n\nhttps://t.me/{bot_name}?start={code}', reply_markup=kb.menu_kb())
            await state.finish()
        elif file_data.split('|')[1] == "голос":
            code = file_data.split('|')[2]
            db.add_new_file_with_password(file_data.split('|')[0], "голос", file_data.split('|')[2], file_data.split('|')[3], message.text)
            await bot.send_message(chat_id=message.chat.id, text=f'Файл был успешно загружен.\n\nПароль: {message.text}\n\nhttps://t.me/{bot_name}?start={code}', reply_markup=kb.menu_kb())
            await state.finish()
        elif file_data.split('|')[1] == "документ":
            code = file_data.split('|')[2]
            db.add_new_file_with_password(file_data.split('|')[0], "документ", file_data.split('|')[2], file_data.split('|')[3], message.text)
            await bot.send_message(chat_id=message.chat.id, text=f'Файл был успешно загружен.\n\nhttps://t.me/{bot_name}?start={code}', reply_markup=kb.menu_kb())
            await state.finish()



@dp.message_handler(state=Info.upload_file, content_types=types.ContentTypes.ANY)
async def upload_file(message: types.Message, state: FSMContext):
    bot_data = await bot.get_me()
    bot_data['username']
    if message.photo:
        fileID = message.photo[-1].file_id
        code = ''.join(random.sample(ascii_letters + digits, random.randint(33, 40)))
        await state.update_data(upload_file_password=f'{message.from_user.id}|фото|{code}|{fileID}')
        await bot.send_message(chat_id=message.chat.id, text='Введи пароль, для файла. Если не хочешь, то напиши "-".', reply_markup=kb.back_kb())
        await Info.upload_file_password.set()
    elif message.text:
        if message.text.lower() == 'отмена':
            await bot.send_message(chat_id=message.chat.id, text='Отменено.', reply_markup=kb.menu_kb())
            await state.finish()
        else:
            await SMF(message)
    elif message.voice:
        fileID = message.voice.file_id
        code = ''.join(random.sample(ascii_letters + digits, random.randint(33, 40)))
        await state.update_data(upload_file_password=f'{message.from_user.id}|голос|{code}|{fileID}')
        await bot.send_message(chat_id=message.chat.id, text='Введи пароль, для файла. Если не хочешь, то напиши "-".', reply_markup=kb.back_kb())
        await Info.upload_file_password.set()
    elif message.video:
        fileID = message.video.file_id
        code = ''.join(random.sample(ascii_letters + digits, random.randint(33, 40)))
        await state.update_data(upload_file_password=f'{message.from_user.id}|видео|{code}|{fileID}')
        await bot.send_message(chat_id=message.chat.id, text='Введи пароль, для файла. Если не хочешь, то напиши "-".', reply_markup=kb.back_kb())
        await Info.upload_file_password.set()
    elif message.document:
        fileID = message.document.file_id
        code = ''.join(random.sample(ascii_letters + digits, random.randint(33, 40)))
        await state.update_data(upload_file_password=f'{message.from_user.id}|документ|{code}|{fileID}')
        await bot.send_message(chat_id=message.chat.id, text='Введи пароль, для файла. Если не хочешь, то напиши "-".', reply_markup=kb.back_kb())
        await Info.upload_file_password.set()

@dp.message_handler(state=Info.delete_file, content_types=types.ContentTypes.TEXT)
async def del_file(message: types.Message, state: FSMContext):
    try:
        number = int(message.text)
        user_data = await state.get_data()
        mess_id = user_data['delete_file']
        all_types, all_ids, passwords = db.get_files_user(message.from_user.id)
        if number > len(all_ids):
            await bot.send_message(chat_id=message.chat.id, text='Такого файла не существует. Введи номер файла:', reply_markup=kb.delete_back())
        else:
            db.delete_file(all_ids[(number-1)][0])
            await bot.delete_message(message.chat.id, mess_id)
            await message.delete()
            await bot.send_message(chat_id=message.chat.id, text='Вы успешно удалили файл!', reply_markup=kb.menu_kb())
            await state.finish()
    except ValueError:
        await bot.send_message(chat_id=message.chat.id, text='Введи номер файла:', reply_markup=kb.delete_back())


@dp.callback_query_handler(state='*')
async def handler_call(call: types.CallbackQuery, state: FSMContext):
    bot_data = await bot.get_me()
    bot_name = bot_data['username']
    chat_id = call.from_user.id
    if call.data == 'delete_file':
        all_types, all_ids, passwords = db.get_files_user(chat_id)
        if all_ids == []:
            await bot.delete_message(chat_id, call.message.message_id)
            await bot.send_message(chat_id=chat_id, text='У вас нету загруженных файлов, чтобы загрузить файлы нажмите "Загрузить файл"', reply_markup = kb.menu_kb())
        else:
            text='Файлы для удаления: \n\n'
            for i, id_file in enumerate(all_ids):
                text+=f'{i+1}. https://t.me/{str(bot_name)}?start={id_file[0]} | {all_types[i][0]} | Пароль: {passwords[i][0]}\n\n'
            text+='Введи номер файла, который ты хочешь удалить.'
            await bot.edit_message_text(chat_id=chat_id, message_id=call.message.message_id, text=text, reply_markup=kb.delete_back())
            await state.update_data(delete_file=call.message.message_id)
            await Info.delete_file.set()
    if call.data == 'delete_back':
        await state.finish()
        all_types, all_ids, passwords = db.get_files_user(chat_id)
        if all_ids == []:
            await bot.delete_message(chat_id, call.message.message_id)
            await bot.send_message(chat_id=chat_id, text='У вас нету загруженных файлов, чтобы загрузить файлы нажмите "Загрузить файл"', reply_markup = kb.menu_kb())
        else:
            text='Ваши файлы: \n\n'
            for i, id_file in enumerate(all_ids):
                text+=f'{i+1}. https://t.me/{str(bot_name)}?start={id_file[0]} | {all_types[i][0]} | Пароль: {passwords[i][0]}\n\n'
            await bot.edit_message_text(chat_id=chat_id, message_id=call.message.message_id, text=text, reply_markup=kb.delete_file())
@dp.message_handler(commands="source")
async def source(message):
    await message.reply("Исходный код:\nhttps://gitlab.com/darkspacer/FSBot")
if __name__ == "__main__":
    db.check_db()
    dp.middleware.setup(ThrottlingMiddleware())
    executor.start_polling(dp, skip_updates=False)
